#lang racket

(require tidyverse
         tidyverse/renderers/plot)

;; SIMPLE SERIES
(define df (make-data-frame))
(define s:rand
  (make-series
   "random"
   #:na (not-a-number)
   #:data
   (list->vector
    (map (λ (n) (random 10 30)) (range 4)))))
(define s:names
  (make-text-series "names" (list "alpha" "bravo" "charlie" "delta")))
(define s:values
  (make-number-series "values" (list 10 20 30 40)))
(define s:fruit
  (make-text-series "fruits" (list "apple" "apple" "orange" "orange")))
(df-add-series df s:names)
(df-add-series df s:values)
(df-add-series df s:rand)
(df-add-series df s:fruit)
(show (tidy-series df #:series "values"))

;; X/Y TESTS
;; https://forge.scilab.org/index.php/p/rdataset/source/tree/master/csv/ggplot2/mpg.csv
;; https://rpubs.com/shailesh/mpg-exploration
(define mpg (read-csv "mpg.csv"))
(show (tidy-scatter mpg
                    #:x-series "cty"
                    #:y-series "hwy"
                    #:color-by "class"
                    #:solid? false
                    ))

(show (tidy-scatter mpg
                    #:x-series "cty"
                    #:y-series "hwy"
                    #:color-by "trans"
                    #:solid? true
                    ))


(define lps (read-csv "https://lynxruf.us/courses/dcs103f18/resources/csv/cleaned-data.csv"))
(save-and-overwrite "age-scatterplot.png"
                    (tidy-series lps
                                 #:series "age"
                                 #:color-by "nationality"
                                 #:solid? true
                                 #:width 1200
                                 #:x-label "Person"
                                 #:y-label "Age"
                                 ))
