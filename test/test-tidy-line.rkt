#lang racket

(require tidyverse
         tidyverse/renderers/plot)

(define mpg (read-csv "mpg.csv"))

; (tidy-line mpg #:series "hwy"  #:annotated? true)
(show (tidy-line mpg
                 #:series "hwy"
                 #:color "green"
                 #:solid? true))

#|
(tidy-line mpg #:series "cty")

(tidy-line mpg #:series "cty"
           #:width 1200
           #:height 50
           #:line-color "gainsboro"
           #:point-color "crimson"
           #:point-radius 6
           #:annotated? true
           #:label "Miles per gallon (city) for vehicles."
           #:label-position 'right)

(tidy-line mpg #:series "cty"
           #:width 1200
           #:height 50
           #:line-color "gainsboro"
           #:point-color "mediumforestgreen"
           #:point-radius 6
           #:annotated? true
           #:label "Miles per gallon (city) for vehicles."
           #:label-position 'left)

(define lps (read-csv "https://lynxruf.us/courses/dcs103f18/resources/csv/cleaned-data.csv"))
(tidy-line lps
           #:width 1000
           #:series "age"
           #:annotated? true
           #:point-color "purple"
           #:label "Ages in the 1913 Lewiston School Census"
           #:label-position 'right)


(save-and-overwrite "age-numberline.png"
                    (tidy-line lps
                               #:width 1000
                               #:series "age"
                               #:annotated? true
                               #:point-color "purple"
                               #:label "Ages in the 1913 Lewiston School Census"
                               #:label-position 'right
                               ))
|#