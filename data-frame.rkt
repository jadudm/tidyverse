#lang racket

(require data-frame/private/df
         tidyverse/series)

(provide df-min
         df-max)

(define (df-min df s)
  (vector-ref
   (vector-sort (df-select df s)
                (extended-series-min (df-get-series df s)))
   0))

(define (df-max df s)
  (vector-ref
   (vector-sort (df-select df s)
                (extended-series-max (df-get-series df s)))
   0))